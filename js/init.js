(function(w){
	var sw = document.body.clientWidth,
		sh = document.body.clientHeight,
		$sgViewport = $('#viewport'),
		$urlToggle = $('#url-toggle'),
		$sizeToggle = $('#size-toggle'),
		$body = $('body'),
		$tSize = $('#size'),
		$sizeS = $('#size-s'),
		$sizeM = $('#size-m'),
		$sizeL = $('#size-l'),
		$sizeXL = $('#size-xl'),
		$sizeFull = $('#size-full'),
		$sizeR = $('#size-random'),
		$sizeDisco = $('#size-disco'),
		$sizeInput = $('#size-enter'),
		$vp,
		$sgPattern,
		discoID = false,
		discoMode,
		keyCodeNumbers = {
			48: "0",
			49: "1",
			50: "2",
			51: "3",
			52: "4",
			53: "5",
			54: "6",
			55: "7",
			56: "8",
			57: "9"
		},
		shiftTimeout,
		declaredSize = "";



	$(w).resize(function(){ //Update dimensions on resize
		sw = document.body.clientWidth;
		sh = document.body.clientHeight;
	});
	
  $(w).keydown(function (a) {
	if (a.shiftKey) {
		clearTimeout(shiftTimeout);
		if ( keyCodeNumbers[a.keyCode] != undefined ) {
			declaredSize += keyCodeNumbers[a.keyCode];
		}
		
  		shiftTimeout = setTimeout( shiftResize, 1000);
	}

    if($body.hasClass("focusMode") && !a.shiftKey ) {
      switch (a.keyCode) {
      case 49:
      case 83:
      $sizeS.click();
      break; 
      case 50:
      case 77:
      $sizeM.click();
      break; 
      case 51:
      case 76:
      $sizeL.click();
      break; 
      case 52:
      case 88:
      $sizeXL.click();
      break; 
      case 53:
      case 70:
      $sizeFull.click();
      break; 
      }
    }
    // alt+U
    if(a.keyCode == 85 && a.altKey) {
      $body.toggleClass("focusMode");
    }
  });
	
	//View Trigger
	$urlToggle.on("click", function(e){
		e.preventDefault();
		$(this).parents('.nav').toggleClass('active');
	});

	//Size Trigger
	$sizeToggle.on("click", function(e){
		e.preventDefault();
		$(this).parents('ul').toggleClass('active');
	});
	
	//Size View Events
	$sizeS.on("click", function(e){
		e.preventDefault();
		killDisco();
		sizeiframe(getRandom(240,500));
	});
	$sizeM.on("click", function(e){
		e.preventDefault();
		killDisco();
		sizeiframe(getRandom(500,800));
	});
	$sizeL.on("click", function(e){
		e.preventDefault();
		killDisco();
		sizeiframe(getRandom(800,1200));
	});
	$sizeXL.on("click", function(e){
		e.preventDefault();
		killDisco();
		sizeiframe(getRandom(1200,1920));
	});
	$sizeFull.on("click", function(e){
		e.preventDefault();
		killDisco();
		sizeiframe(sw);
	});
	$sizeR.on("click", function(e){
		e.preventDefault();
		killDisco();
		sizeiframe(getRandom(240,sw));
	});
	
	$sizeDisco.on("click", function(e){
		e.preventDefault();
		if (discoMode) {
			killDisco();
		} else {
			discoMode = true;
			discoID = setInterval(disco, 800);
		}
		
	});
	
	$sizeInput.submit(function(){
		var val = $('#size-num').val();
		sizeiframe(val);
		return false;
	});


	function shiftResize() {
		if (declaredSize == "") {return false;}
		if (declaredSize < 100) {declaredSize *= 10;}
		if (declaredSize > sw) {declaredSize = sw;} 
		window.location.hash = declaredSize;
		sizeiframe( declaredSize );
		declaredSize = "";
	}

	function disco() {
		sizeiframe(getRandom(240,sw));
	}
	
	function sizeiframe(size) {
		$('#viewport').width(size);
	}
	
	function killDisco() {
		discoMode = false;
		clearInterval(discoID);
		discoID = false;
	}
	
	/* Returns a random number between min and max */
	function getRandom (min, max) {
	    return Math.random() * (max - min) + min;
	}

})(this);